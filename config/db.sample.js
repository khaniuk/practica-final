const { PORT, DATABASE } = require('./constants');

module.exports = {
  urlConn: `mongodb://localhost:${PORT}/${DATABASE}`,
};
