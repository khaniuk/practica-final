# PRACTICA - NODEJS - MODULO II

### Install dependencies

```
yarn install
```

### Routes
***User routes*** (no middleware)
```
localhost:3000/usuarios     // GET
localhost:3000/usuarios      // POST
localhost:3000/usuarios/:id  // UPDATE
localhost:3000/usuarios/:id  // DELETE
```

***Students routes***
```
localhost:3000/estudiantes     // GET
localhost:3000/estudiantes      // POST
localhost:3000/estudiantes/:id  // UPDATE
localhost:3000/estudiantes/:id  // DELETE
```

### Data Base - MongoDB

```
docker pull mongo

docker run --name mymongo -d mongo
```

### Setting DB

change IP Mongo Host and copy files

```
cp constants.sample.js constants.js

cp db.sample.js db.js
```

***Create user***
```
{
	"username": "admin",
	"password": "admin",
	"permissions": [
		"usuarios:list",
		"usuarios:create",
		"usuarios:update",
		"usuarios:delete",
		"estudiantes:list",
		"estudiantes:create",
		"estudiantes:update",
		"estudiantes:delete"
	]
}
```

***Create Student***

Reg 1
```
{
	"nombres": "Gerson",
    "paterno": "Estrada",
    "materno": "Ruiz",
    "cedula": "1234567",
    "edad": 20
}
```

Reg 2
```
{
	"nombres": "Luisa",
    "paterno": "Garnica",
    "materno": "Lopez",
    "cedula": "2345678",
    "edad": 21
}
```
Reg 3
```
{
	"nombres": "Mirtha",
    "paterno": "Martinez",
    "materno": "Torres",
    "cedula": "3456789",
    "edad": 22
}
```

***Edit user permission*** (remove estudiantes:create)
```
{
	"username": "admin",
	"password": "admin",
	"permissions": [
		"usuarios:list",
		"usuarios:create",
		"usuarios:update",
		"usuarios:delete",
		"estudiantes:list",
		"estudiantes:update",
		"estudiantes:delete"
	]
}
```
Los permisos no se tendría que almacenar en el token puesto que va crecer bastante, se haria una consulta en base al ID de usuario y obtener los permisos otorgados.
