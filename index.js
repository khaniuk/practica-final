const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require("body-parser");
const cors = require("cors");
app.use(cors());
const { AuthRoute, StudentRoute, UserRoute } = require("./routes");

// DATABASE
const mongoose = require("mongoose");
const { db } = require("./config");
mongoose.connect(db.urlConn, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(bodyParser.json());

app.use(AuthRoute);
app.use(StudentRoute);
app.use(UserRoute);

app.listen(port, () => {
  console.log(`Start on port ${port}`);
});
