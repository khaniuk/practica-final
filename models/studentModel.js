const mongoose = require("mongoose");
const { Schema } = mongoose;

const StudentSchema = new Schema({
  nombres: String,
  paterno: String,
  materno: String,
  cedula: String,
  edad: Number
});

const Student = mongoose.model("student", StudentSchema);

module.exports = Student;
