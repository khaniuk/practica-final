const mongoose = require("mongoose");
const { Schema } = mongoose;

const UserSchema = new Schema({
  username: String,
  password: String,
  permissions: Array
});

const User = mongoose.model("user", UserSchema);

module.exports = User;
