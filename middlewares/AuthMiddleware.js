const { verify } = require('jsonwebtoken');
const { auth } = require('../config/app');
module.exports =  {
  checkToken: async (req, res, next) => {
    try {
      const { authorization } = req.headers;
      if (!authorization) {
        throw new Error("No auth.");
      }
      const token = authorization.replace('Bearer ', '');
      const tokenDecode = await verify(token, auth.apiKey);
      req.user = tokenDecode;
      next();
    } catch (error) {
      res.status(401).json({
        finalizado: false,
        mensaje: error.message,
        datos: null,
      });
    }
  }
}
