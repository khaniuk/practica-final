module.exports =  {
  checkPermissions: (permission) => async (req, res, next) => {
    try {
      const { permissions } = req.user
      if (!permissions.includes(permission)) {
        throw new Error('Does not have necessary permissions.');
      }
      next()
    } catch (error) {
      res.status(401).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });      
    }
  }
}