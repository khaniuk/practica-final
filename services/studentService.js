const { StudentRepository } = require("../repositories");

exports.findAll = async () => {
  return await StudentRepository.findAndCountAll();
};

exports.createOrUpdate = async (id, data) => {
  return await StudentRepository.createOrUpdate(id, data);
};

exports.remove = async id => {
  return await StudentRepository.remove(id);
};
