const { UserRepository } = require("../repositories");

exports.login = async (username, password) => {
  const exist = await UserRepository.findOne(username, password);
  return exist;
};

exports.findAll = () => {
  return UserRepository.findAndCountAll();
};

exports.createOrUpdate = (id, data) => {
  return UserRepository.createOrUpdate(id, data);
};

exports.remove = id => {
  return UserRepository.remove(id);
};
