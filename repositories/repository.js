exports.findOne = async (model, username, password) => {
  const result = await model.findOne({username, password});
  return result;
};

exports.findAndCountAll = async (model, query) => {
  const count = await model.count(query);
  const rows = await model.find(query);

  return { count, rows };
};

exports.createOrUpdate = async (model, id, data) => {
  let result = null;
  if (id) {
    result = await model.findByIdAndUpdate(id, data);
  } else {
    result = await model.create(data);
  }

  return result;
};

exports.remove = async (model, id) => {
  const result = await model.findByIdAndRemove(id);
  return result;
};
