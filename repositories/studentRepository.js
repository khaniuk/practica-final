const { StudentModel } = require("../models");
const { findAndCountAll, createOrUpdate, remove } = require("./repository");

exports.findAndCountAll = async query => {
  return await findAndCountAll(StudentModel, query);
};

exports.createOrUpdate = async (id, data) => {
  return await createOrUpdate(StudentModel, id, data);
};

exports.remove = async id => {
  return await remove(StudentModel, id);
};
