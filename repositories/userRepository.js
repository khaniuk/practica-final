const { UserModel } = require("../models");
const { findOne, findAndCountAll, createOrUpdate, remove } = require("./repository");

exports.findOne = (username, password) => {
  return findOne(UserModel, username, password);
};

exports.findAndCountAll = query => {
  return findAndCountAll(UserModel, query);
};

exports.createOrUpdate = (id, data) => {
  return createOrUpdate(UserModel, id, data);
};

exports.remove = id => {
  return remove(UserModel, id);
};
