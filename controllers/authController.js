const { sign } = require("jsonwebtoken");
const { auth } = require("../config/app");
const { UserService } = require("../services");

const { msgSuccess, msgError } = require("./response");

const login = async (req, res) => {console.log(req.body);
  try {
    let msg = '';
    const { username, password } = req.body;

    const result = await UserService.login(username, password);
    if (!result) {
      msg = 'User not found';
    }
    console.log(result);
    //const verifyPass = await user.comparePassword(req.body.password);

    const genToken = sign({ id: result.id, username: result.username, permissions: result.permissions }, auth.apiKey, {
      expiresIn: 86400 * 30,
    });
    msg = 'User auth success';

    msgSuccess(res, 200, msg, { id: result.id, username: result.username, permissions: result.permissions, token: genToken });
  } catch (err) {
    msgError(res, 500, err.message, null);
  }
};

module.exports = {
  login
};
