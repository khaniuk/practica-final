const { StudentService } = require("../services");

const { msgSuccess, msgError } = require("./response");

const list = async (req, res) => {
  try {
    const result = await StudentService.findAll();
    msgSuccess(res, 200, "Ok", result);
  } catch (err) {
    msgError(res, 500, err.message);
  }
};

const create = async (req, res) => {
  try {
    const result = await StudentService.createOrUpdate(null, req.body);
    msgSuccess(res, 201, "Created", result);
  } catch (err) {
    msgError(res, 500, err.message, null);
  }
};

const update = async (req, res) => {
  try {
    const result = await StudentService.createOrUpdate(req.params.id, req.body);
    msgSuccess(res, 200, "Updated", result);
  } catch (err) {
    msgError(res, 500, err.message, null);
  }
};

const remove = async (req, res) => {
  try {
    await StudentService.remove(req.params.id);
    msgSuccess(res, 200, "Deleted", null);
  } catch (err) {
    msgError(res, 500, err.message, null);
  }
};

module.exports = {
  list,
  create,
  update,
  remove,
};
