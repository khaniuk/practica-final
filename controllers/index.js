module.exports = {
  AuthController: require("./authController"),
  StudentController: require("./studentController"),
  UserController: require("./userController"),
};
