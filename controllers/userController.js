const { sign } = require("jsonwebtoken");
const { UserService } = require("../services");

const { msgSuccess, msgError } = require("./response");

const list = async (req, res) => {
  try {
    const result = await UserService.findAll();
    msgSuccess(res, 200, "Ok", result);
  } catch (err) {
    msgError(res, 500, err.message);
  }
};

const create = async (req, res) => {
  try {
    const result = await UserService.createOrUpdate(null, req.body);
    msgSuccess(res, 201, "Created", result);
  } catch (err) {
    msgError(res, 500, err.message, null);
  }
};

const update = async (req, res) => {
  try {
    const result = await UserService.createOrUpdate(req.params.id, req.body);
    msgSuccess(res, 200, "Updated", result);
  } catch (err) {
    msgError(res, 500, err.message, null);
  }
};

const remove = async (req, res) => {
  try {
    await UserService.remove(req.params.id);
    msgSuccess(res, 200, "Deleted", null);
  } catch (err) {
    msgError(res, 500, err.message, null);
  }
};

const login = async (req, res) => {
  try {
    const { username, password } = req.body;
    //const verifyPass = await user.comparePassword(req.body.password);

    const genToken = sign({ username, password }, "apikey", {
      expiresIn: 86400 * 30,
    });
    console.log(genToken);
    msgSuccess(res, 200, "Loguin", null);
  } catch (err) {
    msgError(res, 500, err.message, null);
  }
};

module.exports = {
  list,
  create,
  update,
  remove,
  login
};
