exports.msgSuccess = (res, status_code, message, data) => {
  return res.status(status_code).json({ message, data });
};

exports.msgError = (res, status_code, message, data) => {
  return res.status(status_code).json({ message, data });
};
