const express = require("express");
const router = express.Router();
const { UserController } = require("../controllers");
const { AuthMiddleware } = require("../middlewares");

router.get("/usuarios", UserController.list);
router.post("/usuarios", UserController.create);
router.put("/usuarios/:id", UserController.update);
router.delete("/usuarios/:id", UserController.remove);

module.exports = router;
