module.exports = {
  AuthRoute: require("./authRoute"),
  StudentRoute: require("./studentRoute"),
  UserRoute: require("./userRoute"),
};
