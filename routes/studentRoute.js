const express = require("express");
const router = express.Router();
const { StudentController } = require("../controllers");
const { AuthMiddleware, PermissionMiddleware } = require("../middlewares");

router.get("/estudiantes", AuthMiddleware.checkToken, PermissionMiddleware.checkPermissions("estudiantes:list"), StudentController.list);
router.post("/estudiantes", AuthMiddleware.checkToken, PermissionMiddleware.checkPermissions("estudiantes:create"), StudentController.create);
router.put("/estudiantes/:id", AuthMiddleware.checkToken, PermissionMiddleware.checkPermissions("estudiantes:update"), StudentController.update);
router.delete("/estudiantes/:id", AuthMiddleware.checkToken, PermissionMiddleware.checkPermissions("estudiantes:delete"), StudentController.remove);

module.exports = router;
